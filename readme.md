`LEFT`

LEFT est un site présentant des photographies et des peintures.

## Environnement de développement

### Pré-requis

* php 7.4
* Composer
* Symfony CLI
* Docker
* Docker-compose
* Nodejs et yarn

### Lancer l'environnement de développement

```bash
composer install
yarn install
yarn encore dev
docker-compose up -d
symfony serve -d
```

### Ajouter des données de tests

```bash
symfony console doctrine:fixtures:load
```

### Lancer des testes

```bash
php bin/phpunit ---testdox
```

## Production

### Envois des mails de contacts

les mails de prise de contact sont stockés en BDD. Pour les envoyer au peintre par mail, il faut mettre en place un cron sur:

```bash
symfony console app:send-contact
```
