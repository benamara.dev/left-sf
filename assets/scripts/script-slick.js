import "slick-carousel";

$('.slickHome, .slickComment').slick({
    autoplay: true,
    autoplaySpeed: 4000,
    centerPadding: '70px',
    arrows: true,
    adaptiveHeight: true,
    dots: false,
    infinite: true,
    touchThreshold: 100,
    speed: 300,
    slidesToShow: 2,
    slidesToScroll: 2,
    centerMode: true,
    //cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
    prevArrow: '<button class="slick-prev"></button>',
    nextArrow: '<button class="slick-next"></button>',

    responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
                dots: false,
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                dots: false,
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                dots: false,
            }
        }
    ]
});

/* ============================== */

$('.slickCategorie-gallery').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slickCategorie-thumbs'
});

$('.slickCategorie-thumbs').slick({
    centerMode: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    asNavFor: '.slickCategorie-gallery',
    dots: false,
    centerMode: true,
    centerPadding: '110px',
    focusOnSelect: true,

    autoplay: false,
    infinite: true,
    arrows: true,
    prevArrow: '<button class="slick-prev"></button>',
    nextArrow: '<button class="slick-next"></button>',
});




/*======================================== */