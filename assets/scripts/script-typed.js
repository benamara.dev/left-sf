import Typed from 'typed.js';

new Typed(
    '#typed-welcome',{
    strings : ['Développeur Web.','Artiste photographe.','Scrum Master.','Blogger.'],
    delaySpeed : 90,
    cursorChar: "",// character for cursor
    typeSpeed: 60,// typing speed
    startDelay: 600,// time before typing starts
    backSpeed: 20,// backspacing speed
    backDelay: 500,// time before backspacing
    loop: true,// loop
    //loopCount: infinite,// one boucle of 5 = 5
    showCursor: false,// show cursor
    attr: null,// attribute to type (null == text)
    contentType: 'html',// either html or text
    smartBackspace:true,
    },
);

/*===========================================*/
new Typed(
    '#typed-error-404',{
    strings : ['Oops ...','Page non trouvée','ERROR 404.' ],
    typeSpeed : 40,
    backSpeed: 20,
    delaySpeed : 90,
    smartBackspace:true,
    loop : true,
    cursorChar: "|",
    },
);

/* ========================================================= */


