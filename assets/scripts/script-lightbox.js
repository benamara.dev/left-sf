
import lightbox from 'lightbox2';

lightbox.option({
	'albumLabel':	"picture %1 of %2",
	'fadeDuration': 300,
	'resizeDuration': 200,
	'wrapAround': true,
    'fitImagesInViewport': true,
    'imageFadeDuration': 600,
    'showImageNumberLabel': true,

})