import VanillaTilt from 'vanilla-tilt';

const tiltBtn = document.querySelectorAll(".tilt-btn");
const tiltImg = document.querySelectorAll(".tilt-img");

/*=================================== */

VanillaTilt.init(tiltBtn, {
	max: 10,
	scale: 1,
	perspective: 4000,
	reverse: true,
});

/*===================================== */

VanillaTilt.init(tiltImg, {
	max: 10,
	speed: 400,
	scale: 1.05,
	perspective: 1000,
});

/*================================= */

