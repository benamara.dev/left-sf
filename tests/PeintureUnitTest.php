<?php

namespace App\Tests;

use App\Entity\Categorie;
use App\Entity\Commentaire;
use App\Entity\Peinture;
use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;

class PeintureUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $peinture = new Peinture();
        $datetime = new DateTime();
        $categorie = new Categorie();
        $user = new User();

        $peinture->setNom("testnom")
                 ->setLargeur(20.20)
                 ->setHauteur(20.20)
                 ->setEnVente(true)
                 ->setPrix(20.20)
                 ->setDateRealisation($datetime)
                 ->setDescription("testdescription")
                 ->setPortfolio(true)
                 ->setFile("testfile")
                 ->setSlug("testslug")
                 ->setDateAt($datetime)
                 ->setUser($user)
                 ->addCategorie($categorie)
        ;

        $this->assertTrue($peinture->getNom() === "testnom");
        $this->assertTrue($peinture->getLargeur() == 20.20);
        $this->assertTrue($peinture->getHauteur() == 20.20);
        $this->assertTrue($peinture->getEnVente() === true);
        $this->assertTrue($peinture->getPrix() == 20.20);
        $this->assertTrue($peinture->getDateRealisation() === $datetime);
        $this->assertTrue($peinture->getDescription() === "testdescription");
        $this->assertTrue($peinture->getPortfolio() === true);
        $this->assertTrue($peinture->getFile() === "testfile");
        $this->assertTrue($peinture->getSlug() === "testslug");
        $this->assertTrue($peinture->getDateAt() === $datetime);
        $this->assertTrue($peinture->getUser() === $user);
        $this->assertContains($categorie, $peinture->getCategorie());
    }

    public function testIsFalse()
    {
        $peinture = new Peinture();
        $datetime = new DateTime();
        $categorie = new Categorie();
        $user = new User();

        $peinture->setNom("testnom")
                 ->setLargeur(20.20)
                 ->setHauteur(20.20)
                 ->setEnVente(true)
                 ->setPrix(20.20)
                 ->setDateRealisation($datetime)
                 ->setDescription("testdescription")
                 ->setPortfolio(true)
                 ->setFile("testfile")
                 ->setSlug("testslug")
                 ->setDateAt($datetime)
                 ->setUser($user)
                 ->addCategorie($categorie)
        ;

        $this->assertFalse($peinture->getNom() === "testnomfalse");
        $this->assertFalse($peinture->getLargeur() == 21.20);
        $this->assertFalse($peinture->getHauteur() == 21.20);
        $this->assertFalse($peinture->getEnVente() === false);
        $this->assertFalse($peinture->getPrix() == 21.20);
        $this->assertFalse($peinture->getDateRealisation() === new DateTime());
        $this->assertFalse($peinture->getDescription() === "testdescriptionfalse");
        $this->assertFalse($peinture->getPortfolio() === false);
        $this->assertFalse($peinture->getFile() === "testfilefalse");
        $this->assertFalse($peinture->getSlug() === "testslugfalse");
        $this->assertFalse($peinture->getDateAt() === new DateTime());
        $this->assertFalse($peinture->getUser() === new User());
        $this->assertNotContains(new Categorie(), $peinture->getCategorie());
    }

    public function testIsEmpty()
    {
        $peinture = new Peinture();

        $this->assertEmpty($peinture->getNom());
        $this->assertEmpty($peinture->getLargeur());
        $this->assertEmpty($peinture->getHauteur());
        $this->assertEmpty($peinture->getEnVente());
        $this->assertEmpty($peinture->getPrix());
        $this->assertEmpty($peinture->getDateRealisation());
        $this->assertEmpty($peinture->getDescription());
        $this->assertEmpty($peinture->getPortfolio());
        $this->assertEmpty($peinture->getFile());
        $this->assertEmpty($peinture->getSlug());
        $this->assertEmpty($peinture->getDateAt());
        $this->assertEmpty($peinture->getUser());
        $this->assertEmpty($peinture->getCategorie());
        $this->assertEmpty($peinture->getId());
    }

    public function testAddGetRemoveCommentaire()
    {
        //je cree une peinture
        $peinture = new Peinture();
        //je cree un commentaire
        $commentaire = new Commentaire();
        //je verifie le getcommentaires vide
        $this->assertEmpty($peinture->getCommentaires());
        //je verifie le addcommentaire en ajoutant le commentaire cree
        $peinture-> addCommentaire($commentaire);
        //je verifie le getcommentaires: est ce que le commentaire est récupéré
        $this->assertContains($commentaire, $peinture->getCommentaires());
        //je verifie removecommentaire: je supprime le commentaire
        $peinture-> removeCommentaire($commentaire);
        //je verifie le getcommentaire: est ce que  commentaire est vide
        $this->assertEmpty($peinture->getCommentaires());
    }
}
