<?php

namespace App\Tests;

use App\Entity\Blogpost;
use App\Entity\Commentaire;
use App\Entity\Peinture;
use DateTime;
use PHPUnit\Framework\TestCase;

class CommentaireUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $commentaire = new Commentaire();
        $datetime = new DateTime();
        $peinture = new Peinture;
        $blogpost = new Blogpost;

        $commentaire->setAuteur("testauteur")
                    ->setEmail("testemail@test.com")
                    ->setDate($datetime)
                    ->setContenu("testcontenu")
                    ->setPeinture($peinture)
                    ->setBlogpost($blogpost)
        ;

        $this->assertTrue($commentaire->getAuteur() === "testauteur");
        $this->assertTrue($commentaire->getEmail() === "testemail@test.com");
        $this->assertTrue($commentaire->getDate() === $datetime);
        $this->assertTrue($commentaire->getContenu() === "testcontenu");
        $this->assertTrue($commentaire->getPeinture() === $peinture);
        $this->assertTrue($commentaire->getBlogpost() === $blogpost);
    }

    public function testIsFalse()
    {
        $commentaire = new Commentaire();
        $datetime = new DateTime();
        $peinture = new Peinture;
        $blogpost = new Blogpost;

        $commentaire->setAuteur("testauteur")
                    ->setEmail("testemail@test.com")
                    ->setDate($datetime)
                    ->setContenu("testcontenu")
                    ->setPeinture($peinture)
                    ->setBlogpost($blogpost)
        ;

        $this->assertFalse($commentaire->getAuteur() === "testauteurfalse");
        $this->assertFalse($commentaire->getEmail() === "testemailfalse@test.com");
        $this->assertFalse($commentaire->getDate() === new DateTime());
        $this->assertFalse($commentaire->getContenu() === "testcontenufalse");
        $this->assertFalse($commentaire->getPeinture() === new Peinture());
        $this->assertFalse($commentaire->getBlogpost() === new Blogpost());
    }



    public function testIsEmpty()
    {
        $commentaire = new Commentaire();


    

        $this->assertEmpty($commentaire->getAuteur());
        $this->assertEmpty($commentaire->getEmail());
        $this->assertEmpty($commentaire->getDate());
        $this->assertEmpty($commentaire->getContenu());
        $this->assertEmpty($commentaire->getPeinture());
        $this->assertEmpty($commentaire->getBlogpost());
        $this->assertEmpty($commentaire->getid());
    }
}
