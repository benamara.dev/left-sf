<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\Categorie;
use App\Entity\Peinture;

class CategorieUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $categorie = new Categorie();

        $categorie->setNom("testnom")
                  ->setDescription("testdescription")
                  ->setSlug("testslug")
        ;

        $this->assertTrue($categorie->getNom() === "testnom");
        $this->assertTrue($categorie->getDescription() === "testdescription");
        $this->assertTrue($categorie->getSlug() === "testslug");
    }

    public function testIsFalse()
    {
        $categorie = new Categorie();

        $categorie->setNom("testnom")
                  ->setDescription("testdescription")
                  ->setSlug("testslug")
        ;

        $this->assertFalse($categorie->getNom() === "falsenom");
        $this->assertFalse($categorie->getDescription() === "falsedescription");
        $this->assertFalse($categorie->getSlug() === "falseslug");
    }

    public function testIsEmpty()
    {
        $categorie = new Categorie();

        $this->assertEmpty($categorie->getNom());
        $this->assertEmpty($categorie->getDescription());
        $this->assertEmpty($categorie->getSlug());
        $this->assertEmpty($categorie->getId());
    }

     
    public function testAddGetRemovePeinture()
    {
        //je cree une catégorie
        $categorie = new Categorie();
        //je cree une peinture
        $peinture = new Peinture();
        //je verifie le getpeinture vide
        $this->assertEmpty($categorie->getPeintures());
        //je verifie le addpeinture en ajoutant le peinture cree
        $categorie-> addPeinture($peinture);
        //je verifie le getpeinture: est ce que le peinture est récupéré
        $this->assertContains($peinture, $categorie->getPeintures());
        //je verifie removepeinture: je supprime le peinture
        $categorie-> removePeinture($peinture);
        //je verifie le getpeinture: est ce que  peinture est vide
        $this->assertEmpty($categorie->getPeintures());
    }
}
