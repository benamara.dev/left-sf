<?php

namespace App\Tests;

use App\Entity\Blogpost;
use App\Entity\Peinture;
use PHPUnit\Framework\TestCase;
use App\Entity\User;

class UserUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $user = new User();

        $user->setEmail("test@mail.com")
             ->setRoles(["ROLE_TRUE"])
             ->setPassword("testpassword")
             ->setNom("testnom")
             ->setPrenom("testprenom")
             ->setTelephone("123456789")
             ->setInstagram("testinstagram")
             ->setAPropos("testapropos")
        ;

        $this->assertTrue($user->getEmail() === "test@mail.com");
        $this->assertTrue($user->getUserIdentifier() === "test@mail.com");
        $this->assertTrue($user->getRoles() === ["ROLE_TRUE", "ROLE_USER"]);
        $this->assertTrue($user->getPassword() === "testpassword");
        $this->assertTrue($user->getNom() === "testnom");
        $this->assertTrue($user->getPrenom() === "testprenom");
        $this->assertTrue($user->getTelephone() === "123456789");
        $this->assertTrue($user->getInstagram() === "testinstagram");
        $this->assertTrue($user->getAPropos() === "testapropos");
    }

    public function testIsFalse()
    {
        $user = new User();

        $user->setEmail("test@mail.com")
             ->setRoles(["ROLE_TRUE"])
             ->setPassword("testpassword")
             ->setNom("testnom")
             ->setPrenom("testprenom")
             ->setTelephone("123456789")
             ->setInstagram("testinstagram")
             ->setAPropos("testapropos")
        ;
        
        $this->assertFalse($user->getEmail() === "false@mail.com");
        $this->assertFalse($user->getUserIdentifier() === "false@mail.com");
        $this->assertFalse($user->getRoles() === ["ROLE_FALSE", "ROLE_USER"]);
        $this->assertFalse($user->getPassword() === "falsepassword");
        $this->assertFalse($user->getNom() === "falsenom");
        $this->assertFalse($user->getPrenom() === "falseprenom");
        $this->assertFalse($user->getTelephone() === "987654321");
        $this->assertFalse($user->getInstagram() === "falseinstagram");
        $this->assertFalse($user->getAPropos() === "falseapropos");
    }


    public function testIsEmpty()
    {
        $user = new User();

        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getUserIdentifier());
        //$this->assertEmpty($user->getRoles([]));
        $this->assertEmpty($user->getPassword());
        $this->assertEmpty($user->getNom());
        $this->assertEmpty($user->getPrenom());
        $this->assertEmpty($user->getTelephone());
        $this->assertEmpty($user->getInstagram());
        $this->assertEmpty($user->getAPropos());
        $this->assertEmpty($user->getId());
    }

    public function testAddGetRemoveBlogpost()
    {
        //je cree un user
        $user = new User();
        //je cree un blogpost
        $blogpost = new Blogpost();
        //je verifie le getblogpost vide
        $this->assertEmpty($user->getBlogposts());
  
        //je verifie le addblogpost en ajoutant le blogpost cree
        $user-> addBlogpost($blogpost);
        //je verifie le getblogpost: est ce que le blogpost est récupéré
        $this->assertContains($blogpost, $user->getBlogposts());
        //je verifie removeBlogpost: je supprime le blogpost
        $user-> removeBlogpost($blogpost);
        //je verifie le getBlogposts: est ce que Blogposts est vide
        $this->assertEmpty($user->getBlogposts());
    }
  
    public function testAddGetRemovePeinture()
    {
        $user = new User();
        $peinture = new Peinture();
        $this->assertEmpty($user->getPeintures());
  
        $user->addPeinture($peinture);
        $this->assertContains($peinture, $user->getPeintures());
        $user->removePeinture($peinture);
        $this->assertEmpty($user->getPeintures());
    }
}
