<?php

namespace App\Tests;

use App\Entity\Blogpost;
use App\Entity\Commentaire;
use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;

class BlogpostUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $blogpost = new Blogpost();
        $datetime = new DateTime();
        $user = new User();

        $blogpost->setContenu("testcontenu")
                 ->setDate($datetime)
                 ->setSlug("slugtest")
                 ->setTitre("titretest")
                 ->setUser($user)
        ;
        // verifié le asserttrue: est ce que le getuser correspond a $user est
        $this->assertTrue($blogpost->getContenu() === "testcontenu");
        $this->assertTrue($blogpost->getDate() === $datetime);
        $this->assertTrue($blogpost->getSlug() === "slugtest");
        $this->assertTrue($blogpost->getTitre() === "titretest");
        $this->assertTrue($blogpost->getUser() === $user);
    }

    public function testIsFalse()
    {
        $blogpost = new Blogpost();
        $datetime = new DateTime();
        $user = new User();

        $blogpost->setContenu("testcontenu")
                 ->setDate($datetime)
                 ->setSlug("slugtest")
                 ->setTitre("titretest")
                 ->setUser($user)
        ;
        // verifié le assertfalse: est ce que le faux getuser correspond pas a $user (verifier que ca echoue)
        $this->assertFalse($blogpost->getContenu() === "testcontenufalse");
        $this->assertFalse($blogpost->getDate() === new DateTime());
        $this->assertFalse($blogpost->getSlug() === "slugtestfalse");
        $this->assertFalse($blogpost->getTitre() === "titretestfalse");
        $this->assertFalse($blogpost->getUser() === new User());
    }

    public function testIsEmpty()
    {
        $blogpost = new Blogpost();

        $this->assertEmpty($blogpost->getContenu());
        $this->assertEmpty($blogpost->getDate());
        $this->assertEmpty($blogpost->getSlug());
        $this->assertEmpty($blogpost->getTitre());
        $this->assertEmpty($blogpost->getUser());
        $this->assertEmpty($blogpost->getId());
    }

    public function testAddGetRemoveCommentaire()
    {
        //je cree un blogpost
        $blogpost = new Blogpost();
        //je cree un commentaire
        $commentaire = new Commentaire();
        //je verifie le getcommentaire vide
        $this->assertEmpty($blogpost->getCommentaires());

        //je verifie le addcommentaire en ajoutant le commentaire cree
        $blogpost-> addCommentaire($commentaire);
        //je verifie le getcommentaire: est ce que le commentaire est récupéré
        $this->assertContains($commentaire, $blogpost->getCommentaires());
        //je verifie removecommentaire: je supprime le commentaire
        $blogpost-> removeCommentaire($commentaire);
        //je verifie le getcommentaire: est ce que  commentaire est vide
        $this->assertEmpty($blogpost->getCommentaires());
    }
}
