<?php

namespace App\Tests;

use App\Entity\Contact;
use DateTime;
use PHPUnit\Framework\TestCase;

class ContactUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $contact = new Contact();
        $date = new DateTime();
 
        $contact->setNom("truenom")
                ->setEmail("truemail@mail.com")
                ->setMessage("truemessage")
                ->setCreatAt($date)
                ->setIsSend(true)
            ;
        // verifié le asserttrue: est ce que le getnom correspond a $email
        $this->assertTrue($contact->getNom() === "truenom");
        $this->assertTrue($contact->getEmail() === "truemail@mail.com");
        $this->assertTrue($contact->getMessage() === "truemessage");
        $this->assertTrue($contact->getCreatAt() === $date);
        $this->assertTrue($contact->getIsSend() === true);
    }
 
 
    public function testIsFalse()
    {
        $contact = new Contact();
        $date = new DateTime();

        $contact->setNom("truenom")
             ->setEmail("truemail@mail.com")
             ->setMessage("truemessage")
             ->setCreatAt($date)
             ->setIsSend(true)
         ;

        $this->assertFalse($contact->getNom() === "falsenom");
        $this->assertFalse($contact->getEmail() === "falsemail@mail.com");
        $this->assertFalse($contact->getMessage() === "falsemessage");
        $this->assertFalse($contact->getCreatAt() === new DateTime());
        $this->assertFalse($contact->getIsSend() === false);
    }
 
    public function testIsEmpty()
    {
        $contact = new Contact();

        $this->assertEmpty($contact->getNom());
        $this->assertEmpty($contact->getEmail());
        $this->assertEmpty($contact->getMessage());
        $this->assertEmpty($contact->getCreatAt());
        $this->assertEmpty($contact->getIsSend());
        $this->assertEmpty($contact->getId());
    }
}
