<?php

namespace App\Command;

use App\Repository\ContactRepository;
use App\Repository\UserRepository;
use App\Service\ContactService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;

class SendContactCommand extends Command
// creation d une class qui extend Command: cette class s'attend a avoir une fonction execute (voir la ligne 36 )
{
    private $contactRepository;
    private $mailer;
    private $contactService;
    private $userRepository;
    protected static $defaultName = 'app:send-contact';
    //utiliser la variable defaultname pour ecrire la commande [symfony console app:send-contact]
    
    // dans le construct on utilise contactrepository,mailerinterface, contactservice,userrepository
    public function __construct(
        ContactRepository $contactRepository, //recuperer tout les contact qui sont en attente d'envois
        MailerInterface $mailer,//envoyer le mail
        ContactService $contactService,//metrre a jour la partie isSend et persister cette donnee en bdd
        UserRepository $userRepository //envoyer le mail au peintre
    ) {
        $this->contactRepository = $contactRepository;
        $this->mailer = $mailer;
        $this->contactService = $contactService;
        $this->userRepository = $userRepository;
        parent::__construct();
    }
    
    // tous ce qui est dans execute sera lancé suite a la commande
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        // recuperer la liste qui sont en attente d envois, le status de isSend qui est false
        $toSend = $this->contactRepository->findBy(['isSend'=> false]);
        //recupere l adresse du peintre grace a la fonction getPeintre() dans userrepository
        $adress = new Address(
            $this->userRepository->getPeintre()->getEmail(),
            $this->userRepository->getPeintre()->getNom() . ' ' . $this->userRepository->getPeintre()->getPrenom()
        );
        
        //creer une boucle dans le tableau toSend
        foreach ($toSend as $mail) {
            $email = (new Email()) //creer un email
            ->from($mail->getEmail()) //recevoir email du visiteur
            ->to($adress) //envoyer le mail du visiteur a l adresse du peintre
            ->subject('Nouveau message de ' . $mail->getNom())
            //recuperer le nom du visiteur
            ->text($mail->getMessage()); //envoyer le contenu du message

            $this->mailer->send($email); //envoyer le mail en utilisant mailer
            $this->contactService->isSend($mail);
            //utiliser contactservice pour mettre a jour le mail,
            //recuperer les messages en attente dans la base de donnee.
        }
        return Command::SUCCESS; //retour succes de la commande
    }
}
