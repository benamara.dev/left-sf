<?php

namespace App\Controller;

use App\Repository\BlogpostRepository;
use App\Repository\CategorieRepository;
use App\Repository\PeintureRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SitemapController extends AbstractController
{
    /**
     * @Route("/sitemap.{_format}", name="sitemap",
     * format="html",
     * requirements={
     *      "_format": "html|xml",
     *      }
     * )
     */
    public function index(
        Request $request,
        PeintureRepository $peintureRepository,
        BlogpostRepository $blogpostRepository,
        CategorieRepository $categorieRepository
    ): Response {
        $hostname = $request->getSchemeAndHttpHost();
        $urls = [];

        $urls[] = ['loc' => $this->generateUrl('apropos')];
        $urls[] = ['loc' => $this->generateUrl('actualites')];
        $urls[] = ['loc' => $this->generateUrl('contact')];
        $urls[] = ['loc' => $this->generateUrl('home')];
        $urls[] = ['loc' => $this->generateUrl('realisations')];
        $urls[] = ['loc' => $this->generateUrl('portfolio')];

        foreach ($peintureRepository->findall() as $peinture) {
            $urls[] = [
            'loc' => $this->generateUrl('realisations_details', ['slug' => $peinture->getSlug()]),
            'lastmod' => $peinture->getDateAt()->format('Y-m-d')
            ];
        }

        foreach ($blogpostRepository->findall() as $blogpost) {
            $urls[] = [
            'loc' => $this->generateUrl('actualites_details', ['slug' => $blogpost->getSlug()]),
            'lastmod' => $blogpost->getDate()->format('Y-m-d')
            ];
        }

        foreach ($categorieRepository->findall() as $categorie) {
            $urls[] = [
            'loc' => $this->generateUrl('portfolio_categorie', ['slug' => $categorie->getSlug()])
            ];
        }

        $response = new Response(
            $this->renderView('sitemap/index.html.twig', [
            'urls' => $urls,
            'hostname' => $hostname,
            ]),
            200
        );
        $response->headers->set('Content-type', 'text/xml');
            
        return $response;
    }
}
