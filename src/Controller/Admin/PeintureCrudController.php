<?php

namespace App\Controller\Admin;

use App\Admin\Field\VichImageField;
use App\Entity\Peinture;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class PeintureCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Peinture::class;
    }
    /*
    public function configureFields(string $pageName): iterable
    {
        $imageField = TextField::new('imageFile')
                        ->setFormType(VichImageType::class)
                        //->onlyOnForms()
                        ;
        $file = ImageField::new('file')
                        ->setBasePath('/uploads/peintures/')
                        //->onlyOnIndex()
                        ;
        $fields = [
            IntegerField::new('id')->onlyOnIndex(),
            TextField::new('nom'),
            NumberField::new('largeur')->hideOnIndex(),
            NumberField::new('hauteur')->hideOnIndex(),
            BooleanField::new('enVente'),
            NumberField::new('prix')->hideOnIndex(),
            DateTimeField::new('dateRealisation'),
            TextEditorField::new('description')->hideOnIndex(),
            BooleanField::new('portfolio'),

            SlugField::new('slug')  ->setTargetFieldName('nom')
                                    ->hideOnIndex()
                                    ,
            DateTimeField::new('dateAt', 'Date publication'),
            //AssociationField::new('user'),
            AssociationField::new('categorie'),
            //TextField::new('commentaire'),

        ];

        if ($pageName === Crud::PAGE_INDEX || $pageName === Crud::PAGE_DETAIL) {
            $fields[] = $file;
        } else {
            $fields[] = $imageField;
        }



        return $fields;
    }
    */

    public function configureFields(string $pageName): iterable
    {
        return [

            IntegerField::new('id')->onlyOnIndex(),
            TextField::new('nom'),
            NumberField::new('largeur')->hideOnIndex(),
            NumberField::new('hauteur')->hideOnIndex(),
            BooleanField::new('enVente'),
            NumberField::new('prix')->hideOnIndex(),
            DateTimeField::new('dateRealisation'),
            TextEditorField::new('description')->hideOnIndex(),
            BooleanField::new('portfolio'),

            ImageField::new('file')
                ->setBasePath(('/uploads/peintures/'))
                ->onlyOnIndex()
            ,

            TextField::new('imageFile')
            ->setFormType(VichImageType::class)
            ->onlyOnForms()
            ,

            SlugField::new('slug')
                ->setTargetFieldName('nom')
                ->hideOnIndex()
            ,
            DateTimeField::new('dateAt', 'Date publication'),
            //AssociationField::new('user'),
            AssociationField::new('categorie'),
            //TextField::new('commentaire'),


        ];
    }
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setDefaultSort(['dateAt' => 'DESC']);
    }
}
