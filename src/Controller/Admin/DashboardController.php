<?php

namespace App\Controller\Admin;

use App\Entity\Blogpost;
use App\Entity\Categorie;
use App\Entity\Commentaire;
use App\Entity\Peinture;
use App\Entity\User;
use App\Repository\BlogpostRepository;
use App\Repository\CategorieRepository;
use App\Repository\CommentaireRepository;
use App\Repository\ContactRepository;
use App\Repository\PeintureRepository;
use App\Repository\UserRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    protected $BlogpostRepository;
    protected $CategorieRepository;
    protected $CommentaireRepository;
    protected $ContactRepository ;
    protected $PeintureRepository;
    protected $UserRepository;

    public function __construct(
        BlogpostRepository $BlogpostRepository,
        CategorieRepository $CategorieRepository,
        CommentaireRepository $CommentaireRepository,
        ContactRepository $ContactRepository,
        PeintureRepository $PeintureRepository,
        UserRepository $UserRepository
    ) {
        $this->BlogpostRepository = $BlogpostRepository;
        $this->CategorieRepository = $CategorieRepository;
        $this->CommentaireRepository = $CommentaireRepository;
        $this->ContactRepository = $ContactRepository ;
        $this->PeintureRepository = $PeintureRepository;
        $this->UserRepository = $UserRepository;
    }

    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return $this->render('bundles/EasyAdminBundle/welcome.html.twig', [
            
            'countAllBlogpost' => $this->BlogpostRepository->countAllBlogpost(),
            'countAllCategorie' => $this->CategorieRepository->countAllCategorie(),
            'countAllCommentaire' => $this->CommentaireRepository->countAllCommentaire(),
            'countAllContact' => $this->ContactRepository->countAllContact(),
            'countAllPeinture' => $this->PeintureRepository->countAllPeinture(),
            'countAllUser' => $this->UserRepository->countAllUser(),
        
        ]);
    }

    /**
     * @return Dashboard
     */
    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Scrum Coffee Admin')
            ->setFaviconPath('')
            ;
    }

    public function configureAssets(): Assets
    {
        return Assets::new()
            ->addCssFile('bundles/easyadmin/css/fatah-easy-admin.css')
            ->addJsFile('bundles/easyadmin/js/fatah-easy-admin.js')
        ;
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard(
            'Dashboard',
            'fa fa-home'
        );
        yield MenuItem::linkToCrud(
            'Actualiés',
            'fas fa-newspaper',
            Blogpost::class
        );
        yield MenuItem::linkToCrud(
            'Peintures',
            'fas fa-palette',
            Peinture::class
        );
        yield MenuItem::linkToCrud(
            'Carégories',
            'fas fa-tags',
            Categorie::class
        );
        yield MenuItem::linkToCrud(
            'Commentaires',
            'fas fa-comment',
            Commentaire::class
        );
        yield MenuItem::linkToCrud(
            'Utilisateurs',
            'fas fa-users',
            User::class
        );
    }
}
