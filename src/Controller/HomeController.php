<?php

namespace App\Controller;

use App\Repository\BlogpostRepository;
use App\Repository\PeintureRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(PeintureRepository $pr, BlogpostRepository $br): Response
    {
        return $this->render('home/index.html.twig', [
            'peintures' => $pr->lastThreePeinture(),
            'blogposts' => $br->lastThreeBlogpost(),
        ]);
    }
}
