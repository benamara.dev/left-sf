<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Service\ContactService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact")
     */
    public function index(Request $r, ContactService $cs): Response //chercher contactservice
    {


        // Je crée l'objet contact
        $contact = new Contact();

        // Je crée le formulaire et expliquer d'ou il provient et lié à quel objet
        $formContact = $this->createForm(ContactType::class, $contact);

        //verifier les contraints dans la requete
        //Ajouter la signature Request $r
        $formContact->handleRequest($r);

        //traiter les donnés du formulaire
        // est ce que le fomulaire est soumis et valide (est ce qu il y a un token csrf conform) ?

        if ($formContact->isSubmitted() && $formContact->isValid()) {
            $contact = $formContact->getData();
            //executer contactservice
            //utiliser contactservice qui a besoin de la fonction persistecontact() et de son objet $contact
            $cs->persistContact($contact);

            //retourner une vue twig
            //rediriger ver la route contact
            return $this->redirectToRoute('contact');
        }

        return $this->render('contact/index.html.twig', [
            'formContact' => $formContact->createView(),
        ]);
    }
}
