<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AproposController extends AbstractController
{
    /**
     * @Route("/a-propos", name="apropos")
     */
    public function index(UserRepository $ur): Response
    {
        return $this->render('apropos/index.html.twig', [
            'user' => $ur->getpeintre(),
        ]);
    }
}
