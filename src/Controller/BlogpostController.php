<?php

namespace App\Controller;

use App\Entity\Blogpost;
use App\Entity\Commentaire;
use App\Form\CommentaireType;
use App\Repository\BlogpostRepository;
use App\Repository\CommentaireRepository;
use App\Service\CommentaireService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BlogpostController extends AbstractController
{
    /**
     * @Route("/actualites", name="actualites")
     */
    public function actualites(
        BlogpostRepository $br,
        PaginatorInterface $pi,
        Request $req
    ): Response {
        $data = $br->findBy([], ['id' => 'DESC']);
        $actualitesPaginate = $pi->paginate(
            $data,
            $req->query->getInt('page', 1),
            6
        );


        return $this->render('blogpost/actualites.html.twig', [
            'blogposts' => $actualitesPaginate,
        ]);
    }

    /**
     * @Route("/actualites/{slug} ", name="actualites_details")
     */
    public function details(
        Blogpost $b,
        Request $request,
        CommentaireService $commentaireService,
        CommentaireRepository $commentairerepository
    ): Response {
        $commentaires = $commentairerepository->findCommentaire($b);
        // je crée un nouveau commentaire
        $commentaire = new Commentaire();
        //je crée un nouveau formulaire de commentaire
        $formCommentaire = $this->createForm(CommentaireType::class, $commentaire);
        //je recupere le $request
        $formCommentaire->handleRequest($request);
        //si le formulaire est soumis et valide
        //je peux persister les données
        if ($formCommentaire->isSubmitted() && $formCommentaire->isValid()) {
            $commentaire = $formCommentaire->getData();
            //je crée un service dédié a la persistance de commentaires
            $commentaireService->persistCommentaire($commentaire, $b, null);
            return $this->redirectToRoute('actualites_details', ['slug'=> $b->getSlug()]);
        }


        return $this->render('blogpost/details.html.twig', [
            'blogpost'              => $b,
            'formCommentaire'       =>$formCommentaire->createView(),
            'commentaires'          =>$commentaires,
        ]);
    }
}
