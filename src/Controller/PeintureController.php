<?php

namespace App\Controller;

use App\Entity\Commentaire;
use App\Entity\Peinture;
use App\Form\CommentaireType;
use App\Repository\CommentaireRepository;
use App\Repository\PeintureRepository;
use App\Service\CommentaireService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PeintureController extends AbstractController
{
    /**
     * @Route("/realisations", name="realisations")
     */
    public function realisations(
        PeintureRepository $pr,
        PaginatorInterface $pi,
        Request $req
    ): Response {
        $data = $pr->findBy([], ['id' => 'DESC']);
        $peinturesPaginate = $pi->paginate(
            $data,
            $req->query->getInt('page', 1),
            6
        );

        return $this->render('peinture/realisations.html.twig', [
            'peintures' => $peinturesPaginate,
        ]);
    }


    /**
     * @Route("/realisations/{slug} ", name="realisations_details")
     */
    public function details(
        Peinture $p,
        Request $request,
        CommentaireService $commentaireService,
        CommentaireRepository $commentairerepository
    ): Response {
        $commentaires = $commentairerepository->findCommentaire($p);
        $commentaire = new Commentaire();
        $formCommentaire = $this->createForm(CommentaireType::class, $commentaire);
        $formCommentaire->handleRequest($request);
        if ($formCommentaire->isSubmitted() && $formCommentaire->isValid()) {
            $commentaire = $formCommentaire->getData();
            $commentaireService->persistCommentaire($commentaire, null, $p);
            return $this->redirectToRoute('realisations_details', ['slug'=> $p->getSlug()]);
        }
        return $this->render('peinture/details.html.twig', [
            'peinture'              => $p,
            'formCommentaire'       =>$formCommentaire->createView(),
            'commentaires'          =>$commentaires,
        ]);
    }
}
