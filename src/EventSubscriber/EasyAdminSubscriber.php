<?php

namespace App\EventSubscriber;

use App\Entity\Blogpost;
use App\Entity\Peinture;
use DateTime;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Security;

class EasyAdminSubscriber implements EventSubscriberInterface
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => ['setDateAndUser'],
        ];
    }

    public function setDateAndUser(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();
        if (($entity instanceof Blogpost)) {
            $now = new DateTime('now');
            $entity->setDate($now);

            $user = $this->security->getUser();
            $entity->setUser($user);
        }

        if (!($entity instanceof Peinture)) {
            $now = new DateTime('now');
            $entity->setDateAt($now);

            $user = $this->security->getUser();
            $entity->setUser($user);
        }
        
        return;
    }
}
