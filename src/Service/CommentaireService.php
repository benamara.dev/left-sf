<?php

namespace App\Service;

use App\Entity\Blogpost;
use App\Entity\Commentaire;
use App\Entity\Peinture;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

//je créer un service commentaire
class CommentaireService
{
    private $emi;
    private $fbi;

    //chercher entitymanagerinterface et flashbaginterface dans le constructeur
    //entitymanagerinterface: permet de faire persister les données dans bdd
    //flashbaginterface: permet d'afficher un message de success
    public function __construct(EntityManagerInterface $emi, FlashBagInterface $fbi)
    {
        //on met entitymanagerinterface dans la variable emi
        $this->emi =$emi;
        //on met flashbaginterface dans la variable fbi
        $this->fbi =$fbi;
    }

    //creer la fonction persistcommentaire() qui attends un objet Commentaire
    public function persistCommentaire(
        Commentaire $commentaire,
        Blogpost $b = null,
        Peinture $peinture = null
    ): void {
        $commentaire->setIsPublished(false)
                    ->setBlogpost($b)
                    ->setPeinture($peinture)
                    ->setDate(new DateTime('now')) //la date de creation c est maintenant
        ;
        //persister & flush
        $this->emi -> persist($commentaire);
        $this->emi -> flush();
        //envoyer le message flashe
        $this->fbi -> add(
            'success',
            "Votre commentaire est bien envoyé, merci. Il sera publié apres validation"
        );
    }
}
