<?php

namespace App\Service;

use App\Entity\Contact;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

//je créer un service contact
class ContactService
{
    private $emi;
    private $fbi;

    //chercher entitymanagerinterface et flashbaginterface dans le constructeur
    //entitymanagerinterface: permet de faire persister les données dans bdd
    //flashbaginterface: permet d'afficher un message de success
    public function __construct(EntityManagerInterface $emi, FlashBagInterface $fbi)
    {
        //on met entitymanagerinterface dans la variable emi
        $this->emi =$emi;
        //on met flashbaginterface dans la variable fbi
        $this->fbi =$fbi;
    }

    //creer la fonction persistcontact() qui attends un objet Contact
    public function persistContact(Contact $c): void
    {
        $c  ->setIsSend(false) //par defaut le message nest pas envoyé
            ->setCreatAt(new DateTime('now')); //la date de creation c est maintenant
        //persister & flush
        $this->emi -> persist($c);
        $this->emi -> flush();
        $this->fbi -> add('success', 'Votre message est bien envoyé, merci.');//envoyer le message flashe
    }

    public function isSend(Contact $c): void //creer la fonction isSend qui s'attend a lui passer un contact
    {
        $c->setIsSend(true); //set le isSend contact à true
        $this->emi -> persist($c); //envoyer a la bdd
        $this->emi -> flush();
    }
}
