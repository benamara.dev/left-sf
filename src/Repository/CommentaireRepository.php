<?php

namespace App\Repository;

use App\Entity\Blogpost;
use App\Entity\Commentaire;
use App\Entity\Peinture;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Commentaire|null find($id, $lockMode = null, $lockVersion = null)
 * @method Commentaire|null findOneBy(array $criteria, array $orderBy = null)
 * @method Commentaire[]    findAll()
 * @method Commentaire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentaireRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Commentaire::class);
    }


    //faire passer deux valeurs a la fonction
    public function findCommentaire($value)
    {
        if ($value instanceof Blogpost) {
            //si c est un objet de type Blogpost, Peinture
            $object = 'blogpost';
        }
        if ($value instanceof Peinture) {
            $object = 'peinture';
        }
        // la requette s'appel C
        return $this->createQueryBuilder('c')
        // si le champ object soit blogpost ou peinture égale a une valeur
        // set le paramettre de la valeur
            ->andWhere('c.' . $object . ' = :val')
            ->andWhere('c.isPublished = true')
        // la valeur c'est ID
            ->setParameter('val', $value->getId())
        // retourner l'ID et le classer de façon decroissant
            ->orderBy('c.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    //FAIRE LA SOMME DES COMMENTAIRES POUR EA3

    /**
     *
     * @return int/mixed/string
     */
    public function countAllCommentaire()
    {
        return $this->createQueryBuilder('a')
           // ->andWhere('c.exampleField = :val')
           // ->setParameter('val', $value)
           // ->orderBy('c.id', 'ASC')
           // ->setMaxResults(10)
            ->select('COUNT(a.id) as value')
            ->getQuery()
            //->getResult()
            ->getOneOrNullResult()
        ;
    }
}
