<?php
 
namespace App\DataFixtures;

use App\Entity\Blogpost;
use App\Entity\Categorie;
use App\Entity\Peinture;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    //private $encoder;

    //public function __construct(UserPasswordEncoderInterface $encoder)
    //{
    //  $this->encoder = $encoder;
    //}

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        //creat a user
        $user = new User();
 
        $user->setEmail('user@mail.com')
             ->setRoles(['ROLE_PEINTRE'])
             ->setPassword(password_hash("user", PASSWORD_DEFAULT))
             ->setNom($faker->lastName)
             ->setPrenom($faker->FirstName)
             ->setTelephone($faker->e164PhoneNumber)
             ->setInstagram('instagram')
             ->setAPropos($faker->realText(30));
        
        //$password = $this->encoder->encodePassword($user, 'password');
        //$user->setPassword($password);
        
        $manager->persist($user);

        //creat 10 blogpost
        for ($i=0; $i < 10; $i++) {
            $blogpost = new Blogpost();
 
            $blogpost   -> setContenu($faker->realText(30))
                    -> setDate($faker->dateTimeBetween('-6 month', 'now'))
                    -> setSlug($faker->slug(3))
                    -> setTitre($faker->Words(3, true))
                    -> setUser($user); //Relation: le $user est crée en ligne 19
            $manager->persist($blogpost);
        }

        //creat 5 categorie
        for ($j=0; $j < 5; $j++) {
            $categorie = new Categorie();

            $categorie  ->setNom($faker->words(3, true))
                    ->setDescription($faker->sentences(1, true))
                    ->setSlug($faker->slug(3));
            $manager->persist($categorie);

            //creat 2 peinture/categorie (une boucle dans une boucle)

            for ($k=0; $k < 2; $k++) {
                $peinture = new Peinture();
        
                $peinture   ->setNom($faker->words(3, true))
                        ->setLargeur($faker->randomFloat(2, 20, 60))
                        ->setHauteur($faker->randomFloat(2, 20, 60))
                        ->setEnVente($faker->randomElement([true, false]))
                        ->setPrix($faker->randomNumber(5))
                        ->setDateRealisation($faker->dateTimeBetween('-1 years', 'now'))
                        ->setDescription($faker->paragraph(1, true))
                        ->setPortfolio($faker->randomElement([true, false]))
                        ->setFile('peinture.jpg')
                        ->setSlug($faker->slug(3))
                        ->setDateAt($faker->dateTimeBetween('-1 month', 'now'))
                        ->setUser($user)
                        //->addCommentaire('')
                        ->addCategorie($categorie);
                $manager->persist($peinture);
            }
        }

        $manager->flush();
    }
}
