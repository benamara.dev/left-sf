<?php

namespace App\Form;

use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Contrainte;
use Symfony\Component\Form\Extension\Core\Type as Input;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, [
                    //"help" => "Tapez votre nom",
                    "data" => "",
                    "constraints" => [
                                    new Contrainte\NotBlank(["message" => "Vous avez oublié de remplir ce champ"]),
                                    new Contrainte\Length(["min" => 1, "max" => 40,
                                                "minMessage" => "",
                                                "maxMessage" => "Le nom ne doit pas dépasser 40 caractères"])
                    ]])

            ->add('email', EmailType::class, [
                    //"help" => "Tapez votre email",
                    "data" => "",
                    "constraints" => [
                                new Contrainte\NotBlank(["message" => "Vous avez oublié de remplir ce champ"]),
                                new Contrainte\Length(["min" => 3, "max" => 50,
                                                        "minMessage" => "L'email' doit contenir au moins 3 caractères",
                                                        "maxMessage" => "L'email' ne doit pas dépasser 50 caractères"])
                    ]])
            ->add('message', TextareaType::class)
            //->add('creatAt')
            //->add('isSend')
            ->add("envoyer", Input\SubmitType::class, [ "label" => "Envoyer"]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}
