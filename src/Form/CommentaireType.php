<?php

namespace App\Form;

use App\Entity\Commentaire;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints as Contrainte;
use Symfony\Component\Form\Extension\Core\Type as Input;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommentaireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('auteur', TextType::class, [
            //"help" => "Tapez votre nom",
            "data" => "",
            "constraints" => [
                            new Contrainte\NotBlank(["message" => "Vous avez oublié de remplir ce champ"]),
                            new Contrainte\Length(["min" => 1, "max" => 40,
                                        "minMessage" => "",
                                        "maxMessage" => "Le nom ne doit pas dépasser 40 caractères"])
            ]])

            ->add('email', EmailType::class, [
                //"help" => "Tapez votre email",
                "data" => "",
                "constraints" => [
                            new Contrainte\NotBlank(["message" => "Vous avez oublié de remplir ce champ"]),
                            new Contrainte\Length(["min" => 3, "max" => 50,
                                                    "minMessage" => "L'email' doit contenir au moins 3 caractères",
                                                    "maxMessage" => "L'email' ne doit pas dépasser 50 caractères"])
                ]])
            //->add('date')
            ->add('contenu', TextareaType::class)
            //->add('isPublished')
            //->add('peinture')
            //->add('blogpost')
            ->add("soumettre", Input\SubmitType::class, [ "label" => "Soumettre"]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Commentaire::class,
        ]);
    }
}
